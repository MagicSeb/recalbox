//
// Created by bkg2k on 14/10/24.
//

#include "ClockOsd.h"
#include <themes/ThemeManager.h>

ClockOSD::ClockOSD(WindowManager& window, Side side)
  : BaseOSD(window, side, false)
  , mClockFont(Font::get(FONT_SIZE_SMALL, Path(FONT_PATH_REGULAR)))
{
  Vector2f size = mClockFont->sizeText(String(' ').Append(Clock()).Append(' '));
  mClockArea = Rectangle(0, 0, size.x(), size.y());
}

String ClockOSD::Clock()
{
  time_t t = time(nullptr);
  char str[256]; strftime(str, sizeof(str), "%X", localtime(&t));
  return String(str);
}

void ClockOSD::Render(const Transform4x4f& parentTrans)
{
  (void)parentTrans;
  //Renderer::DrawRectangle(mClockArea, 0x000000C0);
  mClockFont->RenderDirect(Clock(), mClockArea.Left(), mClockArea.Top(), 0xFFFFFFFF);
}

