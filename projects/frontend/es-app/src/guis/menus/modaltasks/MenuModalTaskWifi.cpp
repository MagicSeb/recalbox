//
// Created by bkg2k on 13/11/24.
//

#include "MenuModalTaskWifi.h"
#include "recalbox/RecalboxSystem.h"
#include "utils/locale/LocaleHelper.h"
#include "network/Networks.h"

MenuModalTaskWIFI::MenuModalTaskWIFI(WindowManager& window, bool activate)
  : IMenuModalTask<bool, bool>(window, "", activate)
  , mActivate(activate)
{
}

bool MenuModalTaskWIFI::TaskExecute(const bool& activate)
{
  // Disconnect ?
  if (!activate)
  {
    SetText(_("Disconnecting from WIFI..."));
    Thread::Sleep(1000);
    return RecalboxSystem::disableWifi();
  }

  // Connect/Reconnect
  SetText(_("Connecting to WIFI..."));
  RecalboxSystem::disableWifi();
  if (!RecalboxSystem::enableWifi("", "")) return false;
  // No config? exit immediately
  if (RecalboxConf::Instance().GetWifiSSID().empty() || RecalboxConf::Instance().GetWifiKey().empty()) return true;
  // Wait 10s for IP
  for(int i = 15 * (1000 / 20); --i >= 0; Thread::Sleep(20))
  {
    SetText(_("Waiting for IP address... (%is)").Replace("%i", String(i / (1000 / 20))));
    if (Networks::Instance().HasIP(Networks::IPVersion::Both, Networks::Interfaces::Wifi)) return true;
  }
  return false;
}

void MenuModalTaskWIFI::TaskComplete(const bool& result)
{
  if (mActivate)
  {
    bool hasConfig = !RecalboxConf::Instance().GetWifiSSID().empty() &&
                     !RecalboxConf::Instance().GetWifiKey().empty();
    mWindow.displayMessage(result ? _("WIFI CONNECTION OK!") : (hasConfig ? _("WIFI CONNECTION ERROR !\nPlease check your SSID and Password.") : _("WIFI INITIALIZATION FAILURE")));
  }
}
